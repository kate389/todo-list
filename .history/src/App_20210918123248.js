import TodoList from "./components/TodoList";
import Textfield from "@atlaskit/textfield";
import Button from "@atlaskit/button";

function App() {
  return (
    <>
      <h3>Todo List</h3>
      <Textfield
        name="add-todo"
        placeholder="Add items..."
        elemAfterInput={
          <Button isDisabled={true} appreance="primary">
            Add
          </Button>
        }
        css={{ padding: "12px 140px 12px" }}
      ></Textfield>
    </>
  );
}

export default App;
