import React from "react";
import Button from "@atlaskit/button";

export default function TodoList() {
  return (
    <>
      <Button shouldFitContainer>Item 1</Button>
      <Button shouldFitContainer>Item 2</Button>
      <Button shouldFitContainer>Item 3</Button>
    </>
  );
}
