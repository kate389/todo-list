import TodoList from "./components/TodoList";
import Textfield from "@atlaskit/textfield";
import Button from "@atlaskit/button";
import { useState } from "react";

function App() {
  const [todoList, setTodoList] = useState([]);
  const [textInput, setTextInput] = useState("");

  const onTextInputChange = (e) => {
    setTextInput(e.target.value);
  };
  const onAddBtnClick = (e) => {
    // add text input to TodoList
    setTodoList([...todoList, {id:'', name:'TextInput', isCompleted=false}]);
  };

  return (
    <>
      <h3>Todo List</h3>
      <Textfield
        name="add-todo"
        placeholder="Add items..."
        elemAfterInput={
          <Button isDisabled={true} appreance="primary" onClick={onAddBtnClick}>
            Add
          </Button>
        }
        css={{ padding: "2px 4px 2px" }}
        value={textInput}
        onChange={onTextInputChange}
      ></Textfield>
      <TodoList />
    </>
  );
}

export default App;
